import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

items:NbMenuItem[]= [
  {title:'Home', icon:'', link:'/'},
  {title:'About', icon:'', link:'/about'},

]
  
  constructor() { }

  ngOnInit() {
  }

}
