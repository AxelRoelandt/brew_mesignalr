import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';


const routes: Routes = [

  {path: '', component: HomeComponent },// création de la page de home //ATTENTION pas de slash
  {path:'about', component:AboutComponent}// c'est l'url qui va définir la route 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
